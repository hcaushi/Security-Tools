# Web Signer

Script for signing and verifying webpages using OpenPGP using the method described by [Sajal Kayan](https://www.sajalkayan.com/post/pgp-sign-web-content.html), including support for remotely signing messages. Presently this does not verify external resources or images on the webpage, but I hope to include support for them in the future.

## Usage

```
web-sign --sign example.html
web-sign --sign -R /path/to/folder
web-sign --sign -R --remote user@domain:/path/to/folder
web-sign --verify https://caushi.eu
```
