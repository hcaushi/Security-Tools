# Port Scanner

Generic port scanner which can be used to enumerate the subdomains of a domain and list open ports for each subdomain.

## Usage

```
port-scanner <domain>`
```
