# Security Tools

A set of tools and scripts written to aid penetration testing.

These tools are primarily intended to automate tasks which I frequently perform, and to help me get better at Rust – **they are unlikely to be the best tool for any use case**. Regardless, you might find some of these tools useful!

## Tools

The list of tools currently consists of the following:

* [Port scanner](https://gitlab.com/hcaushi/security-tools/-/tree/master/port-scanner)
* [Penetration test report](https://gitlab.com/hcaushi/security-tools/-/tree/master/pentest-report)
